package ru.t1.strelcov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.strelcov.tm.dto.request.TaskBindToProjectRequest;
import ru.t1.strelcov.tm.model.Task;
import ru.t1.strelcov.tm.util.TerminalUtil;

public final class TaskBindToProjectCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-bind-to-project";
    }

    @NotNull
    @Override
    public String description() {
        return "Bind task to project.";
    }

    @Override
    public void execute() {
        @NotNull final ITaskEndpoint projectTaskEndpoint = serviceLocator.getTaskEndpoint();
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER TASK ID:");
        @NotNull final String taskId = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @NotNull final Task task = projectTaskEndpoint.bindTaskToProject(new TaskBindToProjectRequest(getToken(), taskId, projectId)).getTask();
        showTask(task);
    }

}
