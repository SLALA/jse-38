package ru.t1.strelcov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.strelcov.tm.dto.request.ProjectChangeStatusByIdRequest;
import ru.t1.strelcov.tm.model.Project;
import ru.t1.strelcov.tm.util.TerminalUtil;

import static ru.t1.strelcov.tm.enumerated.Status.IN_PROGRESS;

public final class ProjectStartByIdCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-start-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "Start project by id.";
    }

    @Override
    public void execute() {
        @NotNull final IProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        System.out.println("[START PROJECT BY ID]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final Project project = projectEndpoint.changeStatusByIdProject(new ProjectChangeStatusByIdRequest(getToken(), id, IN_PROGRESS.name())).getProject();
        showProject(project);
    }

}
