package ru.t1.strelcov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.api.repository.IProjectRepository;
import ru.t1.strelcov.tm.enumerated.Status;
import ru.t1.strelcov.tm.model.Project;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Optional;

public final class ProjectRepository extends AbstractBusinessRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Override
    @NotNull
    public String getTableName() {
        return "tm_project";
    }

    @SneakyThrows
    @Override
    @NotNull
    public Project fetch(@NotNull final ResultSet result) {
        @NotNull Project project = new Project();
        project.setId(result.getString("id"));
        project.setUserId(result.getString("user_id"));
        project.setName(result.getString("name"));
        project.setDescription(result.getString("description"));
        project.setStatus(Status.valueOf(result.getString("status")));
        project.setCreated(new Date(result.getTimestamp("created").getTime()));
        Optional.ofNullable(result.getTimestamp("start_date")).map(Timestamp::getTime).ifPresent((t) -> project.setDateStart(new Date(t)));
        return project;
    }

    @SneakyThrows
    @Override
    public void add(@NotNull final Project project) {
        @NotNull final String sql =
                "INSERT INTO " + getTableName() + "(id, user_id, name, description, status, created, start_date) VALUES(?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, project.getId());
        statement.setString(2, project.getUserId());
        statement.setString(3, project.getName());
        statement.setString(4, project.getDescription());
        statement.setString(5, project.getStatus().name());
        statement.setTimestamp(6, new Timestamp(project.getCreated().getTime()));
        statement.setTimestamp(7, Optional.ofNullable(project.getDateStart()).map(Date::getTime).map(Timestamp::new).orElse(null));
        statement.executeUpdate();
        statement.close();
    }

    @SneakyThrows
    @Override
    public void update(@NotNull final Project project) {
        @NotNull final String sql =
                "UPDATE " + getTableName() + " SET user_id = ?, name = ?, description = ?, status = ?, created = ?, start_date =? WHERE id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, project.getUserId());
        statement.setString(2, project.getName());
        statement.setString(3, project.getDescription());
        statement.setString(4, project.getStatus().name());
        statement.setTimestamp(5, new Timestamp(project.getCreated().getTime()));
        statement.setTimestamp(6, Optional.ofNullable(project.getDateStart()).map(Date::getTime).map(Timestamp::new).orElse(null));
        statement.setString(7, project.getId());
        statement.executeUpdate();
        statement.close();
    }

}
