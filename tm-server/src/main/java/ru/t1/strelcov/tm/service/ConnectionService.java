package ru.t1.strelcov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.api.service.IConnectionService;
import ru.t1.strelcov.tm.api.service.IPropertyService;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionService implements IConnectionService {

    @NotNull
    private final IPropertyService propertyService;

    public ConnectionService(@NotNull IPropertyService propertyService) {
        this.propertyService = propertyService;
    }

    @SneakyThrows
    @Override
    public @NotNull Connection getConnection() {
        @NotNull final String url = propertyService.getDataBaseUrl();
        @NotNull final String user = propertyService.getDataBaseUserLogin();
        @NotNull final String password = propertyService.getDataBaseUserPassword();
        @NotNull final Connection connection = DriverManager.getConnection(url, user, password);
        connection.setAutoCommit(false);
        return connection;
    }

}
