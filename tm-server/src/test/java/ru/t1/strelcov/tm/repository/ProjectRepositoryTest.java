package ru.t1.strelcov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.strelcov.tm.api.repository.IProjectRepository;
import ru.t1.strelcov.tm.api.repository.IUserRepository;
import ru.t1.strelcov.tm.api.service.IConnectionService;
import ru.t1.strelcov.tm.api.service.IPropertyService;
import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.enumerated.SortType;
import ru.t1.strelcov.tm.model.Project;
import ru.t1.strelcov.tm.model.User;
import ru.t1.strelcov.tm.service.ConnectionService;
import ru.t1.strelcov.tm.service.PropertyService;
import ru.t1.strelcov.tm.util.CheckUtil;

import java.sql.Connection;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ProjectRepositoryTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private Connection connection;

    @NotNull
    private IProjectRepository repository;

    @NotNull
    private IUserRepository userRepository;

    @NotNull
    User testUser = new User("test2", "test", Role.USER);

    @NotNull
    String testUserId = testUser.getId();

    @NotNull
    final List<Project> projects = Arrays.asList(
            new Project(testUserId, "pr1"),
            new Project(testUserId, "pr4"),
            new Project(testUserId, "pr3"));

    @SneakyThrows
    @Before
    public void before() {
        connection = connectionService.getConnection();
        repository = new ProjectRepository(connection);
        userRepository = new UserRepository(connection);
        try {
            Optional.ofNullable(userRepository.findByLogin(testUser.getLogin())).ifPresent((user) -> repository.clear(user.getId()));
            userRepository.removeByLogin(testUser.getLogin());
            userRepository.add(testUser);
            repository.addAll(projects);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            connection.close();
            throw e;
        }
    }

    @SneakyThrows
    @After
    public void after() {
        repository.clear(testUserId);
        connection.commit();
        userRepository.remove(testUser);
        connection.commit();
        connection.close();
    }

    @SneakyThrows
    @Test
    public void addTest() {
        @NotNull final Project project = new Project(testUserId, "pr5");
        int size = repository.findAll(testUserId).size();
        Assert.assertFalse(repository.findAll(testUserId).contains(project));
        repository.add(project);
        connection.commit();
        Assert.assertTrue(repository.findAll(testUserId).contains(project));
        Assert.assertEquals(size + 1, repository.findAll(testUserId).size());
    }

    @SneakyThrows
    @Test
    public void addAllTest() {
        @NotNull final List<Project> list = Arrays.asList(new Project(testUserId, "pr1"), new Project(testUserId, "pr2"));
        int size = repository.findAll(testUserId).size();
        Assert.assertFalse(repository.findAll(testUserId).containsAll(list));
        repository.addAll(list);
        connection.commit();
        Assert.assertTrue(repository.findAll(testUserId).containsAll(list));
        Assert.assertEquals(size + list.size(), repository.findAll(testUserId).size());
    }

    @Test
    public void findByIdTest() {
        for (@NotNull final Project project : repository.findAll(testUserId)) {
            Assert.assertTrue(repository.findAll(testUserId).contains(repository.findById(project.getId())));
            Assert.assertEquals(project, repository.findById(project.getId()));
        }
    }

    @SneakyThrows
    @Test
    public void removeByIdTest() {
        @NotNull final Project project = repository.findAll(testUserId).get(0);
        int fullSize = repository.findAll(testUserId).size();
        Assert.assertNotNull(repository.removeById(project.getId()));
        connection.commit();
        Assert.assertFalse(repository.findAll(testUserId).contains(project));
        Assert.assertEquals(fullSize - 1, repository.findAll(testUserId).size());
    }

    @SneakyThrows
    @Test
    public void clearByUserIdTest() {
        @NotNull Project project = repository.findAll(testUserId).get(0);
        @NotNull final List<Project> list = repository.findAll(testUserId);
        int size = list.size();
        int fullSize = repository.findAll().size();
        repository.clear(testUserId);
        connection.commit();
        Assert.assertEquals(0, repository.findAll(testUserId).size());
        Assert.assertEquals(fullSize - size, repository.findAll().size());
    }

    @Test
    public void findAllByUserIdTest() {
        @NotNull final List<Project> list = repository.findAll().stream().filter((i) -> testUserId.equals(i.getUserId())).collect(Collectors.toList());
        Assert.assertTrue(list.containsAll(repository.findAll(testUserId)));
        Assert.assertEquals(list.size(), repository.findAll(testUserId).size());
    }

    @Test
    public void findAllSortedByUserIdTest() {
        for (@NotNull final SortType sortType : SortType.values()) {
            @NotNull final String sortColumn = sortType.getDataBaseName();
            @NotNull final Comparator<Project> comparator = sortType.getComparator();
            @NotNull final List<Project> sortedList = repository.findAll(testUserId, sortColumn);
            Assert.assertEquals(sortedList.stream().sorted(comparator)
                    .collect(Collectors.toList()), sortedList);
            @NotNull List<Project> list = repository.findAll(testUserId);
            Assert.assertTrue(list.containsAll(sortedList));
            Assert.assertEquals(list.size(), sortedList.size());
        }
    }

    @Test
    public void findByIdByUserIdTest() {
        for (@NotNull final Project project : repository.findAll(testUserId)) {
            Assert.assertEquals(project, repository.findById(project.getUserId(), project.getId()));
        }
    }

    @Test
    public void findByNameByUserIdTest() {
        for (@NotNull final Project project : repository.findAll(testUserId)) {
            if (CheckUtil.isEmpty(project.getName())) continue;
            Assert.assertTrue(repository.findAll(testUserId).contains(repository.findByName(project.getUserId(), project.getName())));
            @Nullable final Project foundEntity = repository.findByName(project.getUserId(), project.getName());
            Assert.assertNotNull(foundEntity);
            Assert.assertEquals(project.getName(), foundEntity.getName());
        }
    }

    @SneakyThrows
    @Test
    public void removeByNameByUserIdTest() {
        @NotNull final Project project = repository.findAll(testUserId).get(0);
        Assert.assertNotNull(project.getName());
        int fullSize = repository.findAll(testUserId).size();
        Optional.ofNullable(repository.removeByName(project.getUserId(), project.getName())).ifPresent(p -> Assert.assertFalse(repository.findAll(testUserId).contains(p)));
        connection.commit();
        Assert.assertEquals(fullSize - 1, repository.findAll(testUserId).size());
    }

    @SneakyThrows
    @Test
    public void removeByIdByUserIdTest() {
        @NotNull final Project project = repository.findAll(testUserId).get(0);
        int fullSize = repository.findAll(testUserId).size();
        @NotNull final String id = project.getId();
        Optional.ofNullable(repository.removeById(testUserId, id)).ifPresent(p -> Assert.assertFalse(repository.findAll().contains(p)));
        connection.commit();
        Assert.assertEquals(fullSize - 1, repository.findAll(testUserId).size());
    }

}
