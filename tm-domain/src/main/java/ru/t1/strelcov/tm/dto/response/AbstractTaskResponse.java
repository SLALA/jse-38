package ru.t1.strelcov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.model.Task;

@Setter
@Getter
@NoArgsConstructor
public abstract class AbstractTaskResponse extends AbstractResultResponse {

    @NotNull
    private Task task;

    public AbstractTaskResponse(@NotNull final Task task) {
        this.task = task;
    }

}
