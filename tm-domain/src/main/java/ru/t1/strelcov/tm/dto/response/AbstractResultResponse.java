package ru.t1.strelcov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
@Getter
@Setter
public abstract class AbstractResultResponse extends AbstractResponse {

    @NotNull
    private Boolean success = true;

    @NotNull
    private String message = "";

    AbstractResultResponse(@NotNull Throwable throwable) {
        success = false;
        message = throwable.getMessage();
    }

}
