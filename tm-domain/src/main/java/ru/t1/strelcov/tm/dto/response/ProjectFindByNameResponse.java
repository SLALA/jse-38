package ru.t1.strelcov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.model.Project;

@Setter
@Getter
@NoArgsConstructor
public final class ProjectFindByNameResponse extends AbstractProjectResponse {

    public ProjectFindByNameResponse(@NotNull final Project project) {
        super(project);
    }

}
