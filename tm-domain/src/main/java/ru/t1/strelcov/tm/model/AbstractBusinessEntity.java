package ru.t1.strelcov.tm.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.entity.IWBS;
import ru.t1.strelcov.tm.enumerated.Status;

import java.util.Date;
import java.util.Objects;

import static ru.t1.strelcov.tm.enumerated.Status.NOT_STARTED;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractBusinessEntity extends AbstractEntity implements IWBS {

    @NotNull
    private String name;

    @Nullable
    private String description;

    @NotNull
    private Status status = NOT_STARTED;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    @NotNull
    private Date created = new Date();

    @NotNull
    private String userId;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    @Nullable
    private Date dateStart;

    public AbstractBusinessEntity(@NotNull String userId, @NotNull String name) {
        this.userId = userId;
        this.name = name;
    }

    public AbstractBusinessEntity(@NotNull String userId, @NotNull String name, @Nullable String description) {
        this.userId = userId;
        this.name = name;
        this.description = description;
    }

    @NotNull
    @Override
    public String toString() {
        return getId() + " : " + name + " : " + created + " : " + userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        AbstractBusinessEntity that = (AbstractBusinessEntity) o;
        return name.equals(that.name) && Objects.equals(description, that.description) && status == that.status && created.equals(that.created) && userId.equals(that.userId) && Objects.equals(dateStart, that.dateStart);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), name, description, status, created, userId, dateStart);
    }

}
