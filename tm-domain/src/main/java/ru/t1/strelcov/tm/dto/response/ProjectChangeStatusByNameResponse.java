package ru.t1.strelcov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.model.Project;

@Setter
@Getter
@NoArgsConstructor
public final class ProjectChangeStatusByNameResponse extends AbstractProjectResponse {

    public ProjectChangeStatusByNameResponse(@NotNull final Project project) {
        super(project);
    }

}
